import logging 
import os 
from  py7zr import SevenZipFile 
import io 
import annautils.jihyocrypt as jihyo

FOXBAT_REMOVE_SOURCE_FILES = bool('FOXBAT_RM_SRC' in os.environ.keys())

def get_redundancy(r):
    
    r = __get_redundancy(r)
    
    MODES = {True : (logging.INFO , 'Using FOXHOUND MODE. JPIC Redundancy will be instilled') , False : ( logging.WARNING , 'USING FOXBAT MODE. NO JPIC REDUNDANCY WILL BE USED') }
    
    logging.log(*MODES[r])
    return r 

def __get_redundancy(r):
    
    global logger 
    
    if r is not None:
        return r 
    
    
    
    if 'FOXBAT' in os.environ : 
    
        if os.environ['FOXBAT'] not in ('0' , '1'):
            raise ValueError("Foxbat Environment Variable can only be 0/1")

        return {'1' : False , '0' : True}[os.environ['FOXBAT']]
            

    elif 'FOXHOUND' in os.environ:

        if os.environ['FOXHOUND'] not in ('0' , '1'):
            raise ValueError("Foxbat Environment Variable can only be 0/1")

        return {'0' : False , '1' : True}[os.environ['FOXBAT']]
            
    else:
        
        logger.critical('No envrionment configuration or status given. DEFAULTING TO FOXHOUND MODE')
        return True 

def create_logger(name = __name__):

    global logger 
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

def find_type(file , mode):

    global logger 

    if not os.path.exists(file):
        raise FileNotFoundError(f"No file by the path {file} exists")

    if mode == 'encrypt':
        return 'encrypt'

    elif mode == 'decrypt':
        return 'decrypt'
    
    elif os.path.isdir(file):
        return 'encrypt'

    elif os.path.splitext(file)[-1] in ('.foxbat' , '.foxhound'):
        return 'decrypt'
    
    elif os.path.exists(file):
        raise Exception("Cannot decipher encryption or decryption mode")

def make_output(ofile , mode , file , redundancy):

    if ofile is not None:
        return ofile 
    
    extension = '.foxhound' if redundancy else '.foxbat'

    if mode == 'encrypt':
        return f'{file}{extension}'

    elif mode == 'decrypt':

        vals = os.path.splitext(file)

        if vals[-1].lower() in ('.foxbat' ,'.foxhound'  ):
            return vals[0]

        else:
            return f'{file}.decrypted'

def encrypt(ifile , ofile , redundancy , password):

    global logger

    logger.debug("setting up handle")
    handle = io.BytesIO()
    
    with SevenZipFile(handle , 'w') as archive:
        archive.writeall(ifile , arcname = '')
    logger.debug("Archived data to memory")

    jihyo.enc_b2f(handle.getvalue() , ofile , password, redundancy , True) 
    logger.info(f"Encrypted {ifile} => {ofile}")

    return True 

def decrypt(ifile , ofile , redundancy , password):

    global logger

    logger.debug(f"Decrypting {ofile}")
    data_zip = jihyo.dec_f2b(ifile , password, redundancy , True)

    logger.debug("Creating memory buffer")
    handle = io.BytesIO(data_zip)#.getbuffer()
    #handle = io.BytesIOWrapper(handle)
    handle = io.BufferedReader(handle )
    

    with SevenZipFile(handle , 'r' ) as archive:
        archive.extractall(ofile) 

    logger.info(f"Decrypted {ifile} to destination {ofile}")
    
    return True 